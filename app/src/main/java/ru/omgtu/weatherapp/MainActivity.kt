package ru.omgtu.weatherapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import kotlinx.coroutines.Job
import ru.omgtu.weatherapp.apiservice.MainRepository
import ru.omgtu.weatherapp.apiservice.RetrofitService
import ru.omgtu.weatherapp.apiservice.models.CurrentModel
import ru.omgtu.weatherapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val retrofitService = RetrofitService.getInstance()
        val mainRepository = MainRepository(retrofitService)
        viewModel = ViewModelProvider(this, MainViewModelFactory(mainRepository)).get(MainViewModel::class.java)

        if(viewModel.settings.value == null) {
            viewModel.initSetings()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.settings.value?.let { viewModel.refresh(it) }
    }

}