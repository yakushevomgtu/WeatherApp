package ru.omgtu.weatherapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.omgtu.weatherapp.apiservice.*
import ru.omgtu.weatherapp.apiservice.models.CurrentModel
import ru.omgtu.weatherapp.apiservice.models.HourlyModel
import ru.omgtu.weatherapp.databinding.MainFragmentLayoutBinding

class MainFragment : Fragment(){

    private val viewModel: MainViewModel by activityViewModels()
    lateinit var binding: MainFragmentLayoutBinding
    lateinit var adapter1: HoursAdapter
    lateinit var adapter2: DaysAdapter

    private val imageMap: Map<String, Int> = mapOf(
        "01d" to R.drawable.ic_01d, "01n" to R.drawable.ic_01n,
        "02d" to R.drawable.ic_02d, "02n" to R.drawable.ic_02n,
        "03d" to R.drawable.ic_03d, "03n" to R.drawable.ic_03n,
        "04d" to R.drawable.ic_04d, "04n" to R.drawable.ic_04n,
        "09d" to R.drawable.ic_09d, "09n" to R.drawable.ic_09n,
        "10d" to R.drawable.ic_10d, "10n" to R.drawable.ic_10n,
        "11d" to R.drawable.ic_11d, "11n" to R.drawable.ic_11n,
        "13d" to R.drawable.ic_13d, "13n" to R.drawable.ic_13n,
        "50d" to R.drawable.ic_50d, "50n" to R.drawable.ic_50n
    )

    private val tempUnitMap: Map<String, Int> = mapOf(
        "cel" to R.drawable.ic_cel, "far" to R.drawable.ic_far
    )


    override fun onCreateView (
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentLayoutBinding.inflate(inflater, container, false)
        val retrofitService = RetrofitService.getInstance()
        val mainRepository = MainRepository(retrofitService)

        adapter1 = HoursAdapter()
        adapter2 = DaysAdapter()

        binding.hourly.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.daily.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.hourly.adapter = adapter1
        binding.daily.adapter = adapter2

        binding.refresh.setOnClickListener{ viewModel.settings.value?.let { it1 ->
            viewModel.refresh(
                it1
            )
        } }

        binding.settings.setOnClickListener { findNavController().navigate(R.id.action_mainFragment2_to_settingsFragment2) }

        viewModel.hourlyWeather.observe(viewLifecycleOwner, Observer{
            adapter1.hours = it
        })

        viewModel.dailyWeather.observe(viewLifecycleOwner, Observer{
            adapter2.days = it
        })

        viewModel.currentWeather.observe(viewLifecycleOwner, Observer{
            renderCurrentWeather(it)
        })
        return binding.root
    }

    private fun renderCurrentWeather(currentWeather: CurrentModel){
        binding.cityName.text = currentWeather.city_name
        binding.curTime.text = currentWeather.cur_time
        binding.curDay.text = currentWeather.cur_day
        binding.weatherDesciption.text = currentWeather.weather_description
        binding.curTemp.text = currentWeather.cur_temp.toString()
        tempUnitMap[currentWeather.temp_unit]?.let { binding.curTempIcon.setImageResource(it) }
        binding.hum.text = currentWeather.cur_humidity
        binding.speed.text = currentWeather.wind_speed
        binding.dirIcon.rotation = 180 + currentWeather.wind_deg
        imageMap[currentWeather.icon_id]?.let { binding.iconId.setImageResource(it) }
    }
}
