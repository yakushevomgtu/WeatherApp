package ru.omgtu.weatherapp.apiservice

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.omgtu.weatherapp.apiservice.models.CoordsModel
import ru.omgtu.weatherapp.apiservice.models.DataModel

interface RetrofitService {
    @GET("data/2.5/forecast?appid=3898f7181e33c1f2e5f1adf1d6f330b8")
    suspend fun getWeather(
        @Query("lang") lang:String,
        @Query("units") units:String,
        @Query("lat") lat:Double,
        @Query("lon") lon:Double
    ): Response<DataModel>

    @GET("geo/1.0/direct?appid=3898f7181e33c1f2e5f1adf1d6f330b8")
    suspend fun getLocation(@Query("q") city: String): List<CoordsModel>

    companion object {
        var retrofitService: RetrofitService? = null
        fun getInstance() : RetrofitService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.openweathermap.org/")
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }

    }
}