package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class CoordsModel(
    @Json(name = "name")
    val city: String,
    @Json(name = "lat")
    val lat: Double,
    @Json(name = "lon")
    val lon: Double
)