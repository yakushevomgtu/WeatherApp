package ru.omgtu.weatherapp.apiservice

import ru.omgtu.weatherapp.apiservice.models.CurrentModel
import ru.omgtu.weatherapp.apiservice.models.DailyModel
import ru.omgtu.weatherapp.apiservice.models.DataModel
import ru.omgtu.weatherapp.apiservice.models.HourlyModel
import kotlin.math.roundToInt

class WeatherService(private val dataModel: DataModel, private val lang: String, private val units: String) {
    fun convertTimeToHour(time: Int): String{
        return "${time%86400/3600}:00"
    }

    private fun convertTimeToDay(time: Int): String{
        return when (time % 604800 / 86400) {
            0 -> if (lang == "ru") "Четверг" else "Thursday"
            1 -> if (lang == "ru") "Пятница" else "Friday"
            2 -> if (lang == "ru") "Суббота" else "Saturday"
            3 -> if (lang == "ru") "Воскресенье" else "Sunday"
            4 -> if (lang == "ru") "Понедельник" else "Monday"
            5 -> if (lang == "ru") "Вторник" else "Tuesday"
            6 -> if (lang == "ru") "Среда" else "Wednesday"
            else -> if (lang == "ru") "Ошибка" else "Error"
        }
    }

    fun getTime(i: Int): Int{
        return dataModel.list[i].dt + (dataModel.city.timezone - 60*60*2)
    }

    fun getCurrentWeather(): CurrentModel {
        return CurrentModel(
            city_name = dataModel.city.name,
            cur_time = convertTimeToHour(getTime(0)),
            cur_day = convertTimeToDay(getTime(0)),
            weather_description = dataModel.list[0].weather[0].description.replaceFirstChar { it.uppercase() },
            cur_temp = dataModel.list[0].main.temp.roundToInt(),
            temp_unit = if(units == "metric") "cel" else "far",
            cur_humidity = "${dataModel.list[0].main.humidity}%",
            wind_speed = "${dataModel.list[0].wind.speed.roundToInt()}"+(if (units == "metric") " met/h" else " mil/h"),
            wind_deg = dataModel.list[0].wind.deg.toFloat(),
            icon_id = dataModel.list[0].weather[0].icon
        )
    }

    fun getHourlyWeather(): MutableList<HourlyModel>{
        val result: MutableList<HourlyModel> = mutableListOf()
        for (i in 1..8){
            val model = HourlyModel(
                time = convertTimeToHour(getTime(i)),
                temp = dataModel.list[i].main.temp.roundToInt(),
                temp_unit = if(units == "metric") "cel" else "far",
                wind_speed = "${dataModel.list[i].wind.speed.roundToInt()}"+(if (units == "metric") " met/h" else " mil/h"),
                wind_deg = dataModel.list[i].wind.deg.toFloat(),
                icon_id = dataModel.list[i].weather[0].icon
            )
            result.add(model)
        }
        return result
    }

    fun getNextDay(): Int{
        var result: Int = 0
        for (i in 0..39){
            if(((getTime(i) % 86400 / 3600) in 12..18) && (convertTimeToDay(getTime(i)) != convertTimeToDay(getTime(0)))){result = i; break;}
        }
        return result
    }

    fun getDailyWeather(): MutableList<DailyModel>{
        val result: MutableList<DailyModel> = mutableListOf()
        for (i in getNextDay()..39 step 8){
            val model = DailyModel(
                day = convertTimeToDay(getTime(i)),
                high_temp = dataModel.list[i].main.temp.roundToInt(),
                temp_unit = if(units == "metric") "cel" else "far",
                low_temp = dataModel.list[i-4].main.temp.roundToInt(),
                icon_id = dataModel.list[i].weather[0].icon
            )
            result.add(model)
        }
        return result
    }
}