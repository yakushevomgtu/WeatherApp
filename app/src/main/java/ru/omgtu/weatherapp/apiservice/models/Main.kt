package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class Main(
    @Json(name = "feels_like")
    val feelsLike: Double,
    @Json(name = "humidity")
    val humidity: Int,
    @Json(name = "temp")
    val temp: Double
)