package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class DataList(
    @Json(name = "dt")
    val dt: Int,
    @Json(name = "main")
    val main: Main,
    @Json(name = "sys")
    val sys: Sys,
    @Json(name = "weather")
    val weather: List<Weather>,
    @Json(name = "wind")
    val wind: Wind
)