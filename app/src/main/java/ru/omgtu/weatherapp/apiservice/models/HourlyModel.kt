package ru.omgtu.weatherapp.apiservice.models

data class HourlyModel (
    val time: String,
    val temp: Int,
    val temp_unit: String,
    val wind_speed: String,
    val wind_deg: Float,
    val icon_id: String
)