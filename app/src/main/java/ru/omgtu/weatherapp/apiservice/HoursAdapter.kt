package ru.omgtu.weatherapp.apiservice

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import okhttp3.internal.notifyAll
import ru.omgtu.weatherapp.R
import ru.omgtu.weatherapp.apiservice.models.HourlyModel
import ru.omgtu.weatherapp.databinding.ItemHourBinding

private val imageMap: Map<String, Int> = mapOf(
    "01d" to R.drawable.ic_01d, "01n" to R.drawable.ic_01n,
    "02d" to R.drawable.ic_02d, "02n" to R.drawable.ic_02n,
    "03d" to R.drawable.ic_03d, "03n" to R.drawable.ic_03n,
    "04d" to R.drawable.ic_04d, "04n" to R.drawable.ic_04n,
    "09d" to R.drawable.ic_09d, "09n" to R.drawable.ic_09n,
    "10d" to R.drawable.ic_10d, "10n" to R.drawable.ic_10n,
    "11d" to R.drawable.ic_11d, "11n" to R.drawable.ic_11n,
    "13d" to R.drawable.ic_13d, "13n" to R.drawable.ic_13n,
    "50d" to R.drawable.ic_50d, "50n" to R.drawable.ic_50n
)

private val tempUnitMap: Map<String, Int> = mapOf(
    "cel" to R.drawable.ic_cel, "far" to R.drawable.ic_far
)

class HoursAdapter: RecyclerView.Adapter<HoursAdapter.HoursViewHolder>() {

    var hours: List<HourlyModel> = emptyList()
        set(newValue){
            field = newValue
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoursViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemHourBinding.inflate(inflater, parent, false)
        return HoursViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HoursViewHolder, position: Int) {
        val hour = hours[position]
        with(holder.binding){
            text.text = hour.time
            imageMap[hour.icon_id]?.let { icon.setImageResource(it) }
            tempUnitMap[hour.temp_unit]?.let { tempIcon.setImageResource(it) }
            temp.text = hour.temp.toString()
            speed.text = hour.wind_speed
            dirIcon.rotation = 180 + hour.wind_deg
        }
    }

    override fun getItemCount(): Int = hours.size

    class HoursViewHolder(
        val binding: ItemHourBinding
    ): RecyclerView.ViewHolder(binding.root)
}