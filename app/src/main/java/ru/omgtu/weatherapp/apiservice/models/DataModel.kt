package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class DataModel(
    @Json(name = "city")
    val city: City,
    @Json(name = "list")
    val list: List<DataList>
)