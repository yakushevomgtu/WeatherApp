package ru.omgtu.weatherapp.apiservice.models

data class DailyModel (
    val day: String,
    val high_temp: Int,
    val temp_unit: String,
    val low_temp: Int,
    val icon_id: String
)