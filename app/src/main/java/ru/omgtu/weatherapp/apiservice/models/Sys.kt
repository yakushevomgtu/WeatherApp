package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class Sys(
    @Json(name = "pod")
    val pod: String
)