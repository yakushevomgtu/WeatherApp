package ru.omgtu.weatherapp.apiservice.models

data class CurrentModel(
    val city_name: String,
    val cur_time: String,
    val cur_day: String,
    val weather_description: String,
    val cur_temp: Int,
    val temp_unit: String,
    val cur_humidity: String,
    val wind_speed: String,
    val wind_deg: Float,
    val icon_id: String
)