package ru.omgtu.weatherapp.apiservice.models


import com.squareup.moshi.Json

data class City(
    @Json(name = "name")
    val name: String,
    @Json(name = "timezone")
    val timezone: Int
)