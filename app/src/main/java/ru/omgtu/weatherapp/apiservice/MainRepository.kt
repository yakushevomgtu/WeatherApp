package ru.omgtu.weatherapp.apiservice

class MainRepository (private val retrofitService: RetrofitService) {

    suspend fun getWeather(lang:String,
                           units:String,
                           lat:Double,
                           lon:Double) = retrofitService.getWeather(lang, units, lat, lon)

    suspend fun getLocation(q: String) = retrofitService.getLocation(q)
}