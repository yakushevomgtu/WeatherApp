package ru.omgtu.weatherapp

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import ru.omgtu.weatherapp.databinding.SettingsFragmentLayoutBinding

class SettingsFragment :Fragment(){
    lateinit var binding: SettingsFragmentLayoutBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SettingsFragmentLayoutBinding.inflate(inflater, container, false)

        binding.editText.setOnEditorActionListener { _, i, _ ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                return@setOnEditorActionListener citySend()
            }
            return@setOnEditorActionListener false
        }

        binding.send.setOnClickListener { citySend() }

        binding.back.setOnClickListener { findNavController().popBackStack() }

        binding.langRadioGroup.setOnClickListener {
            val id = binding.langRadioGroup.checkedRadioButtonId
            viewModel.setLang(binding.langRadioGroup.findViewById<RadioButton>(id).text.toString())
            viewModel.lang_checked_id = id
        }

        binding.unitsRadioGroup.setOnClickListener {
            val id = binding.unitsRadioGroup.checkedRadioButtonId
            viewModel.setUnits(binding.unitsRadioGroup.findViewById<RadioButton>(id).text.toString())
            viewModel.units_checked_id = id
        }

        return binding.root
    }

    private fun citySend():Boolean {
        val keyword = binding.editText.text.toString()
        if (keyword.isBlank()) {
            binding.editText.error = "Keyword is empty"
            return true
        }else{
            viewModel.setCity(keyword)
            return false
        }
    }
}