package ru.omgtu.weatherapp

import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import kotlinx.coroutines.*
import ru.omgtu.weatherapp.apiservice.MainRepository
import ru.omgtu.weatherapp.apiservice.WeatherService
import ru.omgtu.weatherapp.apiservice.models.CurrentModel
import ru.omgtu.weatherapp.apiservice.models.DailyModel
import ru.omgtu.weatherapp.apiservice.models.HourlyModel

class MainViewModel(private val mainRepository: MainRepository): ViewModel() {
    val currentWeather = MutableLiveData<CurrentModel>()
    val hourlyWeather = MutableLiveData<MutableList<HourlyModel>>()
    val dailyWeather = MutableLiveData<MutableList<DailyModel>>()
    val settings = MutableLiveData<Settings>()
    var job: Job? = null
    var lang_checked_id = 0
    var units_checked_id = 0

    fun initSettings(settings: Settings) {
        this.settings.value = settings
    }

    fun initSetings(){
        this.settings.value = Settings(
            city = "omsk",
            lang = "ru",
            units = "metric"
        )
    }

    fun setCity(city: String){
        val oldState = settings.value
        settings.value = oldState?.copy(
            city = city
        )
    }

    fun setLang(lang: String){
        val oldState = settings.value
        settings.value = oldState?.copy(
            lang = lang
        )
    }

    fun setUnits(units: String){
        val oldState = settings.value
        settings.value = oldState?.copy(
            units = units
        )
    }

    fun refresh(settings: Settings){
        job = CoroutineScope(Dispatchers.IO).launch {
            val location = mainRepository.getLocation(settings.city)
            val response = mainRepository.getWeather(settings.lang, settings.units, location[0].lat, location[0].lon)
            withContext(Dispatchers.Main){
                val weatherService = response.body()?.let { WeatherService(it, settings.lang, settings.units) }
                if (weatherService != null) {
                    currentWeather.value = weatherService.getCurrentWeather()
                    hourlyWeather.value = weatherService.getHourlyWeather()
                    dailyWeather.value = weatherService.getDailyWeather()
                }
            }
        }
    }

    data class Settings(
        val city: String,
        val lang: String,
        val units: String
    )

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}
